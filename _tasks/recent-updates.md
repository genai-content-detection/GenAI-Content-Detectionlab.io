---
title: "Recent Updates"
date: 2024-07-08T12:33:46+10:00
featured: true
weight: 3
cat: "agile"
thumb: "../../assets/images/portfolio/portfolio_2col_6.jpg"
fb: "https://facebook.com"
twi: "https://twitter.com"
lin: "https://linkdin.com"
layout: content
---
- 4th January, 2025: Program made available.
- 7th December, 2024: Acceptance notification has been sent.
- 21th October, 2024: Shared task deadlines extended.
- 10th October, 2024: 3rd CFP
- 7th August, 2024: 2nd CFP
- 8th July, 2024: Website is up!
