---
title: "Tasks"
date: 2024-01-01T12:33:46+10:00
featured: true
weight: 1
cat: "agile"
thumb: "../../assets/images/portfolio/portfolio_2col_6.jpg"
fb: "https://facebook.com"
twi: "https://twitter.com"
lin: "https://linkdin.com"
layout: content
---

- [Task 1: Binary Multilingual Machine-Generated Text Detection (Human vs. Machine)](/sharedtasks)
- [Task 2: AI vs. Human -- Academic Essay Authenticity Challenge](/sharedtasks)
- [Task 3: Cross-domain Machine-Generated Text Detection](/sharedtasks)
