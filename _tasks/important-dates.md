---
title: "Important Dates"
date: 2024-07-07T12:33:46+10:00
featured: true
weight: 2
cat: "agile"
thumb: "../../assets/images/portfolio/portfolio_2col_6.jpg"
fb: "https://facebook.com"
twi: "https://twitter.com"
lin: "https://linkdin.com"
layout: content
---


- **Regular Submission Deadline:** November 15, 2024 (dual-submission allowed)
- **Re-submission (with a rebuttal) Deadline:** December 2, 2024
- **Acceptance Notification:** December 7, 2024
- **Camera-Ready Deadline:** December 13, 2024
- **Workshop Day:**	January 19, 2025
- ~~Workshop Day:	January 19-20, 2025~~


**All deadlines are 11:59PM UTC-12:00 ("anywhere on Earth").**
