---
title: "Contact"
date: 2024-01-01T12:33:46+10:00
featured: true
weight: 6
fb: "https://facebook.com"
twi: "https://twitter.com"
lin: "https://linkdin.com"
layout: content
---


Slack: [channel](https://join.slack.com/t/slack-qtp6685/shared_invite/zt-2mm7oskog-H1tW37~ao2TbdOrsJFttoQ)  
Email: [genai-content-detection@googlegroups.com](mailto:genai-content-detection@googlegroups.com)
