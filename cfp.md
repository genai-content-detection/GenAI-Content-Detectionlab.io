---
title: "Call for Papers"
date: 2024-07-07T12:33:46+10:00
featured: true
weight: 7
cat: "agile"
fb: "https://facebook.com"
twi: "https://twitter.com"
lin: "https://linkdin.com"
layout: content
---

### Call for Papers
We seek submissions of long and short papers on original and unpublished work (same page limit as the COLING 2025 main conference). In addition, there will be two shared tasks. All accepted submissions will be presented as talks and/or posters at the workshop, prior to the COLING 2025 main conference.


##### Research Papers
We invite original research papers from a wide range of topics, including but not limited to:

- Detection methods for text, image, speech and other modalities
- Multilingual detection methods
- Detection Methods Image Modality
- Detection Methods Multimodal Content
- Real-time detection systems: real-time systems for detecting AI-generated content in live scenarios.
- Attacks for detection systems
- Datasets and resources
- Benchmarking for AI generated content detection
- AI generated fake news detection
- Deepfakes in audio, videos and images
- Ethical and legal implications of AI generated content

##### Shared Tasks
We plan to run two shared tasks:
- Task 1: Binary Multilingual Machine-Generated Text Detection (Human vs. Machine)
- Task 2: AI vs. Human -- Academic Essay Authenticity Challenge

### Important dates:
- Regular Submission (including shared tasks) Deadline: November 15, 2024 (dual-submission allowed)
- Resubmission (with a rebuttal) Deadline: December 2, 2024
- Acceptance Notification: December 7, 2024
- Camera-Ready Deadline: December 13, 2025
- Workshop Day:	January 19, 2025
- ~~Workshop Day:	January 19-20, 2025~~


***All deadlines are 11:59PM UTC-12:00 (“anywhere on Earth”).***

### Submission Details:
Papers must describe original, completed or in-progress, and unpublished work. All papers will be refereed through a double-blind peer review process by multiple reviewers with final acceptance decisions made by the workshop organizers. Accepted papers will be given up to 9 pages (for full papers), 5 pages (for short papers and posters) in the workshop proceedings, and will be presented as oral paper or poster.

**We are seeking submissions under the following categories:**
<br>
- We are seeking submissions under the following category
- Full/long papers (8 pages)
- Short papers (work in progress, innovative ideas/proposals: 4 pages)
- Shared task papers (4 pages)


Long, short and shared tasks papers must follow the two-column format of *ACL conferences, using the <a href="https://www.overleaf.com/latex/templates/association-for-computational-linguistics-acl-conference/jvxskxpnznfj/" target="_blank">official templates</a>. The templates can be downloaded in style files and formatting. Please do not modify these style files, nor  should you use templates designed for other conferences. Submissions that do not conform to the required styles, including paper size, margin width, and font size restrictions, will be rejected without review. Verification to guarantee conformance to publication standards, we will be using the <a href="https://github.com/acl-org/aclpubcheck" target="_blank">ACL pubcheck tool</a>. The PDFs of camera-ready papers must be run through this tool prior to their final submission, and we recommend its use also at submission time.

Submissions are open to all, and are to be submitted anonymously. For the anonymity, double-blind submission and reproducibility criteria please follow the <a href="https://coling2025.org/calls/main_conference_papers/" target="_blank">COLING 2025 instructions</a>.

**If you have published in *ACL conferences previously, and are interested in helping out in the program committee to review papers, please fill up <a href="https://forms.gle/TRu1A8GaXzRZinDT7" target="_blank">this form</a>.

#### Submission portal
Submissions must be made using the START portal: [https://softconf.com/coling2025/DAIGenC25/](https://softconf.com/coling2025/DAIGenC25/)
