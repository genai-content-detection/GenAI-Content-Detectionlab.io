---
title: "Organizers"
date: 2024-07-07T12:33:46+10:00
featured: true
weight: 7
fb: "https://facebook.com"
twi: "https://twitter.com"
lin: "https://linkdin.com"
layout: content
organizer_title: Workshop Organizers
organizer:
- name: "Firoj Alam"
  thumb: "../assets/images/firoj_web.jpg"
  bio: "Qatar Computing Research Institute, Qatar"
  link: "https://sites.google.com/site/firojalam/"
- name: "Preslav Nakov"
  thumb: "../assets/images/profile_preslav-nakov_secondary.jpg"
  bio: "Mohamed bin Zayed University of Artificial Intelligence, UAE"
- name: "Nizar Habash"
  thumb: "../assets/images/profile_nizar_habash.jpg"
  bio: "New York University Abu Dhabi, UAE"
- name: "Iryna Gurevych"
  thumb: "../assets/images/profile_Iryna_Gurevych.jpg"
  bio: "Mohamed bin Zayed University of Artificial Intelligence, UAE; Technical University of Darmstadt, Germany"
- name: "Shammur Chowdhury"
  thumb: "../assets/images/profile_shammur.jpg"
  bio: "Qatar Computing Research Institute, HBKU, Qatar"
  link: "http://shammur.one/"
- name: "Artem Shelmanov"
  thumb: "../assets/images/artem.jpeg"
  bio: "Mohamed bin Zayed University of Artificial Intelligence, UAE"
- name: "Yuxia Wang"
  thumb: "../assets/images/yuxia.png"
  bio: "Mohamed bin Zayed University of Artificial Intelligence, UAE"
- name: "Ekaterina Artemova"
  thumb: "../assets/images/ekaterina.jpeg"
  bio: "Toloka AI, Netherlands"
- name: "Mucahid Kutlu"
  thumb: "../assets/images/mucahid.jpeg"
  bio: "Qatar University, Qatar"
- name: "George Mikros"
  thumb: "../assets/images/profile_dr-george-mikros_1.jpg"
  bio: "Hamad Bin Khalifa University, Qatar"

program_title: "Program Committee"
program:
- name: "Firoj Alam"
  thumb: "../assets/images/firoj_web.jpg"
  bio: "Qatar Computing Research Institute, Qatar"
  link: "https://sites.google.com/site/firojalam/"
---

{% include organizer.html %}


## Program Committee
- **Liam Dugan**, UCLA, USA
- **Hariram Veeramani**, UCLA, USA
- **Md Tanvirul Alam**, Rochester Institute of Technology, USA
- **Sahinur Rahman Laskar**, UPES, Dehradun, India
- **Somnath Banerjee**, University of Tartu, Estonia
- **Noureldin Elmadany**, Arab Academy for Science and Technology, Egypt
- **Md Saiful Islam**, University of Alberta, USA
- **Abdullah I. Alharbi**, King Abdulaziz University, Saudi Arabia
- **Maram Hasanain**, Qatar Computing Research Institute, Qatar
- **Hind AL-Merekhi**, Qatar Computing Research Institute, Qatar
- **Muhammad Tasnim Mohiuddin**, Qatar Computing Research Institute, Qatar
- **Alham Fikri Aji**, MBZUAI, UAE
- **Zhuohan Xie**, MBZUAI, UAE
- **Tarek Mahmoud**, MBZUAI, UAE
- **Jinyan Su**, Cornell University, USA
- **Rui Xing**, MBZUAI, UAE
- **Jiahui Geng**, MBZUAI, UAE
- **Yassine El Kheir**, Technical University Berlin, Germany
- **Giovanni Puccetti**, Institute of Information Science and Technology "A. Faedo", Italy
- **Vladislav Mikhailov**, University of Oslo, Norway
- **Masahiro Kaneko**, MBZUAI, UAE
- **Ryuto Koike**, Tokyo Institute of Technology, Japan
- **Orchid Chetia Phukan**, Indraprastha Institute of Information Technology Delhi, India
- **Koel Dutta Chowdhury**,	Saarland University, Germany
- **Saied Alshahrani**, Clarkson University, USA
- **Fahad Shamshad**, MBZUAI, UAE
- **Md Messal Monem Miah**, Texas A&M University, USA
- **Shah Nawaz**, Johannes Kepler University Linz, Austria
- **Dr. Bishwa Ranjan Das**, Interscience Institute of Management and Technology, India
- **Nada Ayman GabAllah**, Coventry University, Egypt Branch, Egypt
- **Kamel Gaanoun**, Institut National de Statistiques et d'Economie Appliquée, Morocco
- **Shailja Thakur**, IBM Research, USA
- **Amr Keleg**, University of Edinburgh, UK
- **Akim Tsvigun**, University of Amsterdam, Netherlands
- **Muhammad Amin Nadim**, University of Pegaso, Italy
- **Marc Franco-Salvador**,	Symanto Research, Germany
- **José Ángel González**, Symanto Research, Germany
- **Areg Mikael Sarvazyan**, Symanto Research, Germany
- **Walter Daelemans**, University of Antwerp, Belgium
- **Piotr Przybyła**, Universitat Pompeu Fabra, Italy
- **Adaku Uchendu**, 	MIT Lincoln Laboratory, USA 
- **Sheily Panwar**,	CUC-Ulster University (Doha), Qatar
- **Xianjun Yang**,	UCSB, USA 
- **Fatima Haouari**,	Qatar University
- **Fahim Shakil Tamim**, IUBAT, Bangladesh
- **Zach Johnson**,	Microsoft, USA
- **Abubakarr Jaye**,	Microsoft, USA 
- **Jason Lucas**, Penn State University, USA
- **Francisco Rangel**, Genaios, Germany
- **Erum Haris**,	University of Leeds, UK
- **Mohammad Ruhul Amin**, Fordham University, USA
- **Ram Mohan Rao Kadiyala**, University of Maryland, USA
- **Nadia Ghezaiel**, University of Hail, Saudi Arabia
- **Md Towhidul Absar Chowdhury**, Rochester Institute of Technology, USA
- **Dominik Macko**, Kempelen Institute of Intelligent Technologies (KInIT), Slovakia

[ACL Anti-harassment Policy](https://www.aclweb.org/adminwiki/index.php?title=Anti-Harassment_Policy)
