---
layout: page
title: "Accepted Papers"
permalink: /accpted_papers
order : 4
---


## Accepted Papers and Proceeding

### Proceeding
Proceeding at [ACL Anthology](https://aclanthology.org/volumes/2025.genaidetect-1/)

### List of Accepted Papers

| Title | Authors | Paper Type |
|---|---|---|
| SilverSpeak: Evading AI-Generated Text Detectors using Homoglyphs | Aldan Creo and Shushanta Pudasaini | Research paper |
| [Human vs. AI: A Novel Benchmark and a Comparative Study on the Detection of Generated Images and the Impact of Prompts](assets/presentations/17_Human_vs_AI_Benchmark_and_Study_on_Detection_of_Images_and_Impact_of_Prompts.pdf)
 | Philipp Moeßner and Heike Adel | Research paper |
| [Mirror Minds: An Empirical Study on Detecting LLM-Generated Text via LLMs](assets/presentations/MirrorMinds.pdf) | Josh Baradia, Shubham Gupta, and Suman Kundu | Research paper |
| [Benchmarking AI Text Detection: Assessing Detectors Against New Datasets, Evasion Tactics, and Enhanced LLMs](assets/presentations/27_ShushantaPudasainiCOLING.pdf) | Shushanta Pudasaini, Luis Miralles, David Lillis, and Marisa Llorens Salvador | Research paper |
| [Cross-table Synthetic Tabular Data Detection](assets/presentations/29_Cross_Table_Tab_Data_Detection.pdf) | G. Charbel N. Kindji, Lina M. Rojas Barahona, Elisa Fromont, and Tanguy Urvoy | Research paper |
| Your Large Language Models are Leaving Fingerprints | Hope Elizabeth McGovern, Rickard Stureborg, Yoshi Suhara, and Dimitris Alikaniotis | Research paper |
| [GPT-4 is Judged More Human than Humans in Displaced and Inverted Turing Tests](assets/presentations/40_GPT-4_is_Judged_More_Human_than_Humans_in_Inverted_Displaced_TT.pdf) | Ishika M. Rathi, Sydney Taylor, Benjamin Bergen, and Cameron Jones | Research paper |
| [The Consistent Lack of Variance of Psychological Factors Expressed by LLMs and Spambots](assets/presentations/43_ConsistentLack.pdf) | Vasudha Varadarajan, Salvatore Giorgi, Siddharth Mangalik, Nikita Soni, Dave M. Markowitz, and H. Andrew Schwartz | Research paper |
| [DAMAGE: Detecting Adversarially Modified AI Generated Text](assets/presentations/44_ DAMAGE_Presentation.pdf) | Elyas Masrour, Bradley N. Emi, and Max Spero | Research paper |
| Text Graph Neural Networks for Detecting AI-Generated Content | Andric Valdez and Helena Gomez-Adorno | Research paper |
| I Know You Did Not Write That! A Sampling Based Watermarking Method for Identifying Machine Generated Text | Kaan Efe Keleş, Ömer Kaan Gürbüz, and Mucahid Kutlu | Research paper |
| DCBU at GenAI Detection Task 1: Enhancing Machine-Generated Text Detection with Semantic and Probabilistic Features | Zhaowen Zhang, Songhao Chen, and Bingquan Liu | Shared Task 1 |
| L3i++ at GenAI Detection Task 1: Can Label-Supervised LLaMA Detect Machine-Generated Text? | Hanh Thi Hong Tran and NGUYEN Tien Nam | Shared Task 1 |
| TechExperts(IPN) at GenAI Detection Task 1: Detecting AI-Generated Text in English and Multilingual Contexts | Gull Mehak, Amna Qasim, Abdul Gafar Manuel Meque, Nisar Hussain, Grigori Sidorov, and Alexander Gelbukh | Shared Task 1 |
| [SzegedAI at GenAI Detection Task 1: Beyond Binary - Soft-Voting Multi-Class Classification for Binary Machine-Generated Text Detection Across Diverse Language Models](assets/presentations/14_SzegedAI.pdf) | Mihaly Kiss and Gábor Berend | Shared Task 1 |
| Team Unibuc - NLP at GenAI Detection Task 1: Qwen it detect machine-generated text? | Claudiu Creanga, Teodor-George Marchitan, and Liviu P. Dinu | Shared Task 1 |
| Fraunhofer SIT at GenAI Detection Task 1: Adapter Fusion for AI-generated Text Detection | Karla Schaefer and Martin Steinebach | Shared Task 1 |
| OSINT at GenAI Detection Task 1: Multilingual MGT Detection: Leveraging Cross-Lingual Adaptation for Robust LLMs Text Identification | Shifali Agrahari and Sanasam Ranbir Singh | Shared Task 1 |
| Nota AI at GenAI Detection Task 1: Unseen Language-Aware Detection System for Multilingual Machine-Generated Text | Hancheol Park, Jaeyeon Kim, Geonmin Kim, and Tae-Ho Kim | Shared Task 1 |
| CNLP-NITS-PP at GenAI Detection Task 1: AI-Generated Text Using Transformer-Based Approaches | ANNEPAKA YADAGIRI, sai teja lekkala, Mandadoddi Srikar Vardhan, Partha Pakray, and Reddi Mohana Krishna | Shared Task 1 |
| LuxVeri at GenAI Detection Task 1: Inverse Perplexity Weighted Ensemble for Robust Detection of AI-Generated Text across English and Multilingual Contexts | MD. Kamrujjaman Mobin and Md Saiful Islam | Shared Task 1 |
| Grape at GenAI Detection Task 1: Leveraging Compact Models and Linguistic Features for Robust Machine-Generated Text Detection | Nhi Hoai Doan and Kentaro Inui | Shared Task 1 |
| AI-Monitors at GenAI Detection Task 1: Fast and Scalable Machine Generated Text Detection | Azad Singh, Vishnu Tripathi, Ravindra Kumar Pandey, Pragyanand Saho, Prakhar Joshi, Neel Mani, Richa Alagh, Pallaw Mishra, and Piyush Arora | Shared Task 1 |
| [Advacheck at GenAI Detection Task 1: AI Detection Powered by Domain-Aware Multi-Tasking](assets/presentations/51_Advacheck.pdf) | German Gritsai, Anastasia Voznuyk, Ildar Khabutdinov, and Andrey Grabovoy | Shared Task 1 |
| [GenAI Content Detection Task 1: English and Multilingual Machine-Generated Text Detection: AI vs. Human](assets/presentations/GenAI_Content_Detection_Task_1.pdf) | Yuxia Wang, Artem Shelmanov, Jonibek Mansurov, and others | Shared Task 1 |
| [IntegrityAI at GenAI Detection Task 2: Detecting Machine-Generated Academic Essays in English and Arabic Using ELECTRA and Stylometry](assets/presentations/13-Mohammad_ALSMADi-IntegrityAI.pdf) | Mohammad ALSmadi | Shared Task 2 |
| EssayDetect at GenAI Detection Task 2: Guardians of Academic Integrity: Multilingual Detection of AI-Generated Essays | Shifali Agrahari, Subhashi Jayant, Saurabh Kumar, and Sanasam Ranbir Singh | Shared Task 2 |
| CNLP-NITS-PP at GenAI Detection Task 2: Leveraging DistilBERT and XLM-RoBERTa for Multilingual AI-Generated Text Detection | ANNEPAKA YADAGIRI, Reddi Mohana Krishna, and Partha Pakray | Shared Task 2 |
| RA at GenAI Detection Task 2: Fine-tuned Language Models For Detection of Academic Authenticity, Results, and Thoughts | Rana Gharib and Ahmed Elgendy | Shared Task 2 |
| Tesla at GenAI Detection Task 2: Fast and Scalable Method for Detection of Academic Essay Authenticity | Vijayasaradhi Indurthi and Vasudeva Varma | Shared Task 2 |
| [GenAI Content Detection Task 2: AI vs. Human – Academic Essay Authenticity Challenge](assets/presentations/COLING-2025-GenAI-workshop_task2.pdf) | Shammur Absar Chowdhury, Hind Almerekhi, Mucahid Kutlu, Kaan Efe Keleş, Fatema Ahmad, Tasnim Mohiuddin, George Mikros and Firoj Alam | Shared Task 2 |
| [CNLP-NITS-PP at GenAI Detection Task 3: Cross-Domain Machine-Generated Text Detection Using DistilBERT Techniques](assets/presentations/09_Task3_CNLP_NITS_PP1.pdf) | sai teja lekkala, ANNEPAKA YADAGIRI, Mangadoddi Srikar Vardhan, and Partha Pakray | Shared Task 3 |
| Leidos at GenAI Detection Task 3: A Weight-Balanced Transformer Approach for AI Generated Text Detection Across Domains | Abishek R. Edikala, Gregorios A. Katsios, Noelie Creaghe, and Ning Yu | Shared Task 3 |
| Pangram at GenAI Detection Task 3: An Active Learning Approach to Machine-Generated Text Detection | Bradley N. Emi, Max Spero, and Elyas Masrour | Shared Task 3 |
| LuxVeri at GenAI Detection Task 3: Cross-Domain Detection of AI-Generated Text Using Inverse Perplexity-Weighted Ensemble of Fine-Tuned Transformer Models | MD. Kamrujjaman Mobin and Md Saiful Islam | Shared Task 3 |
| [BBN-U.Oregon's ALERT system at GenAI Content Detection Task 3: Robust Authorship Style Representations for Cross-Domain Machine-Generated Text Detection](assets/presentations/37-BBNUOALERT.pdf) | Hemanth Kandula, CHAK FAI LI, Haoling Qiu, Damianos Karakos, Hieu Man, Thien Huu Nguyen, and Brian Ulicny | Shared Task 3 |
| Random at GenAI Detection Task 3: A Hybrid Approach to Cross-Domain Detection of Machine-Generated Text with Adversarial Attack Mitigation | Shifali Agrahari, Prabhat Mishra, and Sujit Kumar | Shared Task 3 |
| MOSAIC at GENAI Detection Task 3: Zero-Shot Detection Using an Ensemble of Models | Matthieu Dubois, François Yvon, and Pablo Piantanida | Shared Task 3 |
| [GenAI Content Detection Task 3: Cross-Domain Machine Generated Text Detection Challenge](assets/presentations/54_GenAITask3.pdf) | Liam Dugan, Andrew Zhu, Firoj Alam, Preslav Nakov, Marianna Apidianaki, and Chris Callison-Burch | Shared Task 3 |
